# a-john conda recipe

Home: "https://gitlab.esss.lu.se/nice/conda-bot-testbed/testbed-modules/a-john"

Package license: EPICS Open License

Recipe license: BSD 3-Clause

Summary: test module a
